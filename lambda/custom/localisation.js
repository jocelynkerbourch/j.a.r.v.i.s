module.exports = {
    en: {
        translation: {
            WELCOME_MSG: 'What can I do for you?',
            UNSUPPORTED_DEVICE_MSG: 'Sorry. This device does not support dynamic location updates. You can try opening the skill from the Alexa app to get location data.',
            PERMISSION_CARD_MSG: 'I would like to use your location. To turn on location sharing, please go to your Alexa app, and follow the instructions. ',
            LOCATION_DISABLED: 'Please make sure device location tracking is enabled in your device. ',
            LOCATION_NOT_RUNNING: 'I having trouble accessing your location. Please wait a moment, and try again later. ',
            LOCATION_ERROR: 'There was an error accessing your location. Please try again later. ',
            LOCATION_INACCURATE: 'Sorry. Could not find an accurate location.',
            LOCATION_MSG: `Your position {{count}} second ago was <say-as interpret-as="cardinal">{{latitude}}</say-as> degrees {{cardinalLat}} and <say-as interpret-as="cardinal">{{longitude}}</say-as> degrees {{cardinalLon}}. `,
            LOCATION_MSG_plural: `Your position {{count}} seconds ago was <say-as interpret-as="cardinal">{{latitude}}</say-as> degrees {{cardinalLat}} and <say-as interpret-as="cardinal">{{longitude}}</say-as> degrees {{cardinalLon}}. `,
            ALTITUDE_MSG: `You're <say-as interpret-as="cardinal">{{count}}</say-as> meter above sea level. `,
            ALTITUDE_MSG_plural: `You're <say-as interpret-as="cardinal">{{count}}</say-as> meters above sea level. `,
            SPEED_MSG: 'Your current speed is <say-as interpret-as="cardinal">{{count}}</say-as> kilometer per hour. ',
            SPEED_MSG_plural: 'Your current speed is <say-as interpret-as="cardinal">{{count}}</say-as> kilometers per hour. ',
            HEADING_MSG: `And your heading towards {{heading}}`,
            CARDINAL_NORTH: 'north',
            CARDINAL_SOUTH: 'south',
            CARDINAL_EAST: 'east',
            CARDINAL_WEST: 'west',
            CARDINAL_BY: 'by',
            HELP_MSG: 'You can say tell me my location! How can I help?',
            GOODBY_MSG: 'Goodbye!',
            ERROR_MSG: 'Sorry, there was an error. Please try again.'
        },
        voice:"Matthew"
    },
    fr: {
        translation: {
            WELCOME_MSG: 'Que puis-je faire pour vous ? ',
            UNSUPPORTED_DEVICE_MSG: 'Désolé. Cet appareil ne prend pas en charge les mises à jour d\'emplacement dynamiques. Vous pouvez essayer d\'ouvrir la compétence depuis l\'application Alexa pour obtenir des données de localisation.',
            PERMISSION_CARD_MSG: 'J\'aimerai utiliser votre emplacement. Pour activer le partage de position, accédez à votre application Alexa et suivez les instructions. ',
            LOCATION_DISABLED: 'Assurez-vous que le suivi de la localisation de l\'appareil est activé sur votre appareil. ',
            LOCATION_NOT_RUNNING: 'J\'ai du mal à accéder à votre emplacement. Veuillez patienter un instant et réessayer plus tard ',
            LOCATION_ERROR: 'Une erreur s\'est produite lors de l\'accès à votre emplacement. Veuillez réessayer plus tard. ',
            LOCATION_INACCURATE: 'Pardon. Impossible de trouver un emplacement précis.',
            LOCATION_MSG: `Votre position il y a {{count}} seconde était de <say-as interpret-as="cardinal">{{latitude}}</say-as> degrés {{cardinalLat}} et <say-as interpret-as="cardinal">{{longitude}}</say-as> degrés {{cardinalLon}}. `,
            LOCATION_MSG_plural: `Votre position il y a {{count}} secondes était de <say-as interpret-as="cardinal">{{latitude}}</say-as> degrés {{cardinalLat}} et <say-as interpret-as="cardinal">{{longitude}}</say-as> degrés {{cardinalLon}}. `,
            ALTITUDE_MSG: `Vous êtes à <say-as interpret-as="cardinal">{{count}}</say-as> mètre au dessus du niveau de la mer. `,
            ALTITUDE_MSG_plural: `Vous êtes <say-as interpret-as="cardinal">{{count}}</say-as> mètres au dessus du niveau de la mer. `,
            SPEED_MSG: 'Votre vitesse actuelle est <say-as interpret-as="cardinal">{{count}}</say-as> kilomètre par heure. ',
            SPEED_MSG_plural: 'Votre vitesse actuelle est <say-as interpret-as="cardinal">{{count}}</say-as> kilomètres par heure.',
            HEADING_MSG: `Et votre direction est {{heading}}`,
            CARDINAL_NORTH: 'nord',
            CARDINAL_SOUTH: 'sud',
            CARDINAL_EAST: '<phoneme alphabet="ipa" ph="ɛst">est</phoneme>',
            CARDINAL_WEST: 'ouest',
            CARDINAL_BY: 'par',
            HELP_MSG: 'Vous pouvez dire dites-moi ma position! Comment puis-je aider ?',
            GOODBY_MSG: 'Au revoir !',
            ERROR_MSG: 'Désolé, il y a eu une erreur. Veuillez réessayer.'
        },
        voice:"Mathieu"
    }
  }