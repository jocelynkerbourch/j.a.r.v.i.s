const Alexa = require('ask-sdk-core');
const i18n = require('i18next');
const languageStrings = require('./localisation');

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'LaunchRequest';
    },
    handle(handlerInput) {
        const speechText = switchVoice(handlerInput.t('WELCOME_MSG'),handlerInput);
        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};
const MyLocationIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'MyLocationIntent';
    },
    handle(handlerInput) {
        const {context} = handlerInput.requestEnvelope;
        const {request} = handlerInput.requestEnvelope;
        const isGeoSupported = context.System.device.supportedInterfaces.Geolocation;

        let speechText = handlerInput.t('UNSUPPORTED_DEVICE_MSG');

        if (isGeoSupported) {
            var geoObject = context.Geolocation;
            let ACCURACY_THRESHOLD = 100;

            // check if there's permission to get location updates
            if ( ! geoObject || ! geoObject.coordinate ) {
                var skillPermissionGranted = context.System.user.permissions.scopes['alexa::devices:all:geolocation:read'].status === "GRANTED";
                if ( !skillPermissionGranted) {
                  return handlerInput.responseBuilder
                    .speak(handlerInput.t('PERMISSION_CARD_MSG'))
                    .withAskForPermissionsConsentCard(['alexa::devices:all:geolocation:read'])
                    .getResponse();
                } else {
                    if(context.Geolocation.locationServices.access !== 'ENABLED'){
                        return handlerInput.responseBuilder
                            .speak(handlerInput.t('LOCATION_DISABLED'))
                            .getResponse();
                    }
                    if(context.Geolocation.locationServices.status !== 'RUNNING'){
                        return handlerInput.responseBuilder
                            .speak(handlerInput.t('LOCATION_NOT_RUNNING'))
                            .getResponse();
                    }
                    return handlerInput.responseBuilder
                        .speak(handlerInput.t('LOCATION_ERROR'))
                        .getResponse();
                }
            }

            // fetch geolocation data and use it in the response
            if (geoObject && geoObject.coordinate && geoObject.coordinate.accuracyInMeters < ACCURACY_THRESHOLD ) {
                let freshness = (new Date(request.timestamp) - new Date(geoObject.timestamp)) / 1000; // freshness in seconds

                const lat = geoObject.coordinate.latitudeInDegrees;
                const lon = geoObject.coordinate.longitudeInDegrees;
                const coordinateAccuracy = geoObject.coordinate.accuracyInMeters;
                const altitude = geoObject.altitude ? geoObject.altitude.altitudeInMeters : null;
                const altitudeAccuracy = geoObject.altitude ? geoObject.altitude.accuracyInMeters : null;
                let heading = geoObject.heading.directionInDegrees;
                let speed = geoObject.heading.speedInMetersPerSecond;
                const cardinalLat = lat >= 0 ? handlerInput.t('CARDINAL_NORTH') : handlerInput.t('CARDINAL_SOUTH');
                const cardinalLon = lon >= 0 ? handlerInput.t('CARDINAL_EAST') : handlerInput.t('CARDINAL_WEST');

                speechText = handlerInput.t('LOCATION_MSG', {count: freshness, latitude: lat.toFixed(2), cardinalLat: cardinalLat, longitude: lon.toFixed(2), cardinalLon: cardinalLon, interpolation: { escapeValue: false }});
                if(altitude)
                    speechText += handlerInput.t('ALTITUDE_MSG', {count: altitude.toFixed(2)}); //TODO detect unit and convert accordingly
                if(speed > 0){
                    speechText += handlerInput.t('SPEED_MSG', {count: speed * 3.6}); //convert to km/h (TODO detect unit and convert accordingly)
                    speechText += handlerInput.t('HEADING_MSG', {heading: getHeading(heading, handlerInput), interpolation: { escapeValue: false }});
                }
            } else {
                speechText = handlerInput.t('LOCATION_INACCURATE');
            }
        }

        return handlerInput.responseBuilder
            .speak(switchVoice(speechText,handlerInput))
            .getResponse();
    }
};
const HelpIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.HelpIntent';
    },
    handle(handlerInput) {
        const speechText =switchVoice(handlerInput.t('HELP_MSG'),handlerInput);

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};
const CancelAndStopIntentHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'IntentRequest'
            && (handlerInput.requestEnvelope.request.intent.name === 'AMAZON.CancelIntent'
                || handlerInput.requestEnvelope.request.intent.name === 'AMAZON.StopIntent');
    },
    handle(handlerInput) {
        const speechText = switchVoice(handlerInput.t('GOODBYE_MSG'),handlerInput);
        return handlerInput.responseBuilder
            .speak(speechText)
            .getResponse();
    }
};
const SessionEndedRequestHandler = {
    canHandle(handlerInput) {
        return handlerInput.requestEnvelope.request.type === 'SessionEndedRequest';
    },
    handle(handlerInput) {
        return handlerInput.responseBuilder.getResponse();
    }
};

const ErrorHandler = {
    canHandle() {
        return true;
    },
    handle(handlerInput, error) {
        console.error(`Error handled: ${error.message}`);
        const speechText = switchVoice(handlerInput.t('ERROR_MSG'),handlerInput);

        return handlerInput.responseBuilder
            .speak(speechText)
            .reprompt(speechText)
            .getResponse();
    }
};

const LocalisationRequestInterceptor = {
  process(handlerInput) {
      const locale = handlerInput.requestEnvelope.request.locale;
      const lng = locale.substring(0, 2);
      i18n.init({
          lng: locale,
          resources: languageStrings
      }).then((t) => {
          handlerInput.t = (...args) => t(...args);
      });

      if (languageStrings.hasOwnProperty(lng)){
        handlerInput.voice = languageStrings[lng].voice;
      }else{
        handlerInput.voice = languageStrings["en"].voice;
      }
      
  }
};

function getHeading(degrees, handlerInput) {
  let input = (degrees / 11.25)+.5|0;
  input = input > 31.5 ? 0 : input;
  let j = input % 8;
  input = (input / 8)|0 % 4;
  let cardinal = [handlerInput.t('CARDINAL_NORTH'), handlerInput.t('CARDINAL_EAST'), handlerInput.t('CARDINAL_SOUTH'), handlerInput.t('CARDINAL_WEST')];
  let pointDesc = ['1', `1 ${handlerInput.t('CARDINAL_BY')} 2`, '1-C', `C ${handlerInput.t('CARDINAL_BY')} 1`, 'C', `C ${handlerInput.t('CARDINAL_BY')} 2`, '2-C', `2 ${handlerInput.t('CARDINAL_BY')} 1`];
  let str1, str2, strC, result;

  str1 = cardinal[input];
  str2 = cardinal[(input + 1) % 4];
  strC = (str1 === cardinal[0] || str1 === cardinal[2]) ? str1 + str2 : str2 + str1;
  result = pointDesc[j].replace('1', str1).replace('2', str2).replace('C', strC);
  return result;
}

function switchVoice(text,handlerInput) {
  const voice = handlerInput.voice;
  if (text){
    return `<speak><voice name='${voice}'>${text}</voice></speak>`
  }
}

exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        LaunchRequestHandler,
        MyLocationIntentHandler,
        HelpIntentHandler,
        CancelAndStopIntentHandler,
        SessionEndedRequestHandler)
    .addErrorHandlers(
        ErrorHandler)
    .addRequestInterceptors(LocalisationRequestInterceptor)
    .withCustomUserAgent('jarvis/v1')
    .lambda();
